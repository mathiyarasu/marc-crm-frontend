$('[data-toggle="collapse"]').on('click', function(){
  if ($(this).attr('aria-expanded') == 'true') {
      $(this).find('i').addClass('fa-angle-up').removeClass('fa-angle-down');
  } else {
      $(this).find('i').addClass('fa-angle-down').removeClass('fa-angle-up');
  }
});