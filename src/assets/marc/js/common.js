/*------ popup middle ---------*/

    $(document).ready(function(){
        function alignModal(){
            var modalDialog = $(this).find(".modal-dialog");
            modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
        }
        $(".modal").on("shown.bs.modal", alignModal);

        $(window).on("resize", function(){
            $(".modal:visible").each(alignModal);
        });   
    });

/*------ dashboard active ---------*/

    $(document).ready(function() {
        $(".sidebar .nav li").click(function () {
            $(".sidebar .nav li").removeClass("active");
            $(this).addClass("active");     
        });
    
    });


    /*------ seachbox show ---------*/
   
    $(document).ready(function () {
    
        $('#custom-search-input .search-query').click(function (event) {
            $(".instant-results").fadeIn('slow').css('height', 'auto');
            event.stopPropagation();
        });

        $('body').click(function () {
            $(".instant-results").fadeOut('500');
        });
    });

    
    /*------ data user active class jquery ---------*/

    $(document).ready(function() {
        $(".data-user-list li a").click(function () {
            $(".data-user-list li a").removeClass("data-list-active");
            $(this).addClass("data-list-active");     
        });
    
    });