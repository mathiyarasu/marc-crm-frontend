import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { Login } from '../model/login';
import { NotificationService } from '../services/notification.service'
import { Constants } from '../services/constants'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginUrl = "user/login";
  loginForm:FormGroup;
  submitted = false;
 
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      userPhone:["",[Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      userPassword: ['', [Validators.required,Validators.minLength(6)]],
    })
  }

  get formControl(){
    return this.loginForm.controls;
  }

  authentication() {
      const apiUrl = Constants.baseUrl + this.loginUrl;
      this.http.post(apiUrl, this.loginForm.value).subscribe(
        (data) => {
          if (data['status'] === 'success') {
            this.notifyService.showSuccess(data['message'], "");
            localStorage.setItem('userName', window.btoa(data['payload'].userName));
            localStorage.setItem('userId', window.btoa(data['payload'].userId));
            localStorage.setItem('userPhone', window.btoa(data['payload'].userPhone));
            localStorage.setItem('token', window.btoa(data['payload'].token));
            this.router.navigateByUrl('/home')
          } else {
            this.notifyService.showError(data['message'], "");
          }
        },
        (err) => {
        }
      )
  }
}
