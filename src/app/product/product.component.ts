import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../services/notification.service'
import { Constants } from '../services/constants';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public getAllProductsUrl = "/product/getAllProducts";
  model: any=[];
  constructor(
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService) { }

  ngOnInit() {
    this.getAllProducts();
  }
  getAllProducts() {
    const apiUrl = Constants.baseUrl + this.getAllProductsUrl;
    this.http.get(apiUrl).subscribe(
      (data)=>{
        if(data['status'] === 'success'){
          this.model = data['payload'];
        }
      }
    )
  }
}