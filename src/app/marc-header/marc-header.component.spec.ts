import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcHeaderComponent } from './marc-header.component';

describe('MarcHeaderComponent', () => {
  let component: MarcHeaderComponent;
  let fixture: ComponentFixture<MarcHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
