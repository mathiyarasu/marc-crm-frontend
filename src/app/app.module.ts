import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { NotificationService } from '../app/services/notification.service'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductCheckoutComponent } from './product-checkout/product-checkout.component';
import { CartComponent } from './cart/cart.component';
import { OrderTrackComponent } from './order-track/order-track.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AddProductComponent } from './add-product/add-product.component';
import { WhislistComponent } from './whislist/whislist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MarcHeaderComponent } from './marc-header/marc-header.component';
import { MarcSidebarComponent } from './marc-sidebar/marc-sidebar.component';
import { MarcLeadinboxComponent } from './marc-leadinbox/marc-leadinbox.component';
import { MarcLeadcreateComponent } from './marc-leadcreate/marc-leadcreate.component';
import { MarcTestComponent } from './marc-test/marc-test.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ProductComponent,
    ProductDetailsComponent,
    ProductCheckoutComponent,
    CartComponent,
    OrderTrackComponent,
    ContactUsComponent,
    AddProductComponent,
    WhislistComponent,
    DashboardComponent,
    MarcHeaderComponent,
    MarcSidebarComponent,
    MarcLeadinboxComponent,
    MarcLeadcreateComponent,
    MarcTestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [ {provide: NotificationService, useClass: NotificationService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
