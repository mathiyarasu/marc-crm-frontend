import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcLeadinboxComponent } from './marc-leadinbox.component';

describe('MarcLeadinboxComponent', () => {
  let component: MarcLeadinboxComponent;
  let fixture: ComponentFixture<MarcLeadinboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcLeadinboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcLeadinboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
