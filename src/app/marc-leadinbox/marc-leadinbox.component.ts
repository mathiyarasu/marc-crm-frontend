import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../services/notification.service'
import { Constants } from '../services/constants';
import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-marc-leadinbox',
  templateUrl: './marc-leadinbox.component.html',
  styleUrls: ['./marc-leadinbox.component.css']
})
export class MarcLeadinboxComponent implements OnInit {
  public getAllLeadUrl = "web/v1/lead/getlead";
  model: any=[];
  constructor(
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService) { }

  ngOnInit() {
    this.getAllLead();

  }
  getAllLead() {
    const apiUrl = Constants.baseUrl + this.getAllLeadUrl;
    this.http.get(apiUrl).subscribe(
      (data)=>{
        if(data['status'] === 'success'){
          this.model = data['payload'];
          console.log("getAllLead--------------------->",JSON.stringify(this.model));
        }
      }
    )
  }
}
