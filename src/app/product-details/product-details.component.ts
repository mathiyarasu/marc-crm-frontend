import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../services/notification.service'
import { Constants } from '../services/constants';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  public productId = "";
  public productUrl = "/product/getProduct/";
  public allListOfalues = "/product/getAllListOfalues"
  model: any = [];
  lov: any = [];
  public lovId:number;
  lovObj: any = [];
  constructor(
    private router: Router,
    private routerId: ActivatedRoute,
    private http: HttpClient,
    private notifyService: NotificationService) { }


  ngOnInit() {
    this.productId = this.routerId.snapshot.paramMap.get("id");
    this.getProduct(this.productId);
    this.getAllListOfalues();
  }
  getProduct(productId) {
    const apiUrl = Constants.baseUrl + this.productUrl + productId;
    this.http.get(apiUrl).subscribe(
      (data) => {
        if (data['status'] === 'success') {
          this.model = data['payload'];
          this.lovId = this.model.productCatName;
          //console.log("======>", JSON.stringify(this.model))
        }
      }
    )
  }

  getAllListOfalues() {
    const apiUrl = Constants.baseUrl + this.allListOfalues;
    this.http.get(apiUrl).subscribe(
      (data) => {
        if (data['status'] === 'success') {
          this.lov = data['payload'];
          this.lovObj = this.lov.filter(list => list.lovId === Number(this.lovId));
          this.model.productCatName = this.lovObj[0].lovName;
        }
      }
    )
  }

  addToCart() {
    this.model.cart = true;
  }
}
