import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductCheckoutComponent } from './product-checkout/product-checkout.component';
import { CartComponent } from './cart/cart.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { OrderTrackComponent } from './order-track/order-track.component';
import { AddProductComponent } from './add-product/add-product.component';
import { WhislistComponent } from './whislist/whislist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MarcLeadinboxComponent } from './marc-leadinbox/marc-leadinbox.component';
import { MarcLeadcreateComponent } from './marc-leadcreate/marc-leadcreate.component';
import { MarcTestComponent } from './marc-test/marc-test.component';





const routes: Routes = [
  { path: 'login', component :LoginComponent },
  { path: 'home', component :HomeComponent },
  { path: '', component :HomeComponent },
  { path: 'register', component :RegisterComponent},
  { path: 'products-list', component :ProductComponent },
  { path: 'product-details/:id', component :ProductDetailsComponent },
  { path: 'product-checkout', component :ProductCheckoutComponent},
  { path: 'cart', component :CartComponent },
  { path: 'contact', component :ContactUsComponent },
  { path: 'track', component :OrderTrackComponent },
  { path: 'add-product', component : AddProductComponent},
  { path: 'whishlist', component : WhislistComponent},
  { path: 'dashboard', component : DashboardComponent},
  { path: 'leadinbox', component : MarcLeadinboxComponent},
  { path: 'leadcreate', component : MarcLeadcreateComponent},
  { path: 'test', component : MarcTestComponent}






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
