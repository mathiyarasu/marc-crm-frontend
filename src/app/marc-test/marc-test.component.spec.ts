import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcTestComponent } from './marc-test.component';

describe('MarcTestComponent', () => {
  let component: MarcTestComponent;
  let fixture: ComponentFixture<MarcTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
