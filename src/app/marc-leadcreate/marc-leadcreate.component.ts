import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import { NotificationService } from '../services/notification.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Constants } from '../services/constants';
@Component({
  selector: 'app-marc-leadcreate',
  templateUrl: './marc-leadcreate.component.html',
  styleUrls: ['./marc-leadcreate.component.css']
})
export class MarcLeadcreateComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  public registerUrl = "web/v1/lead/addlead";
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService) { }
    
  ngOnInit() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      busiDesc:  ['', Validators.required],
      email:  ['', [Validators.required,Validators.email]],
      mobile:  ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      city:  ['', Validators.required],
      postcode:  ['', [Validators.required,Validators.minLength(6),Validators.maxLength(6)]]


    })

    
  }

  get formControl(){
    return this.registerForm.controls;
  }
  register() {
    
    this.submitted = true;
    console.log("----------------------->")

    const apiUrl = Constants.baseUrl + this.registerUrl;
    console.log("----------------------->",JSON.stringify(this.registerForm.value))
    this.http.post(apiUrl,this.registerForm.value).subscribe(
      (data)=>{
        if(data['status'] === 'success'){
          this.notifyService.showSuccess(data['message'],"");
          this.router.navigateByUrl("/login");
        }
      }
    )
  }

}
