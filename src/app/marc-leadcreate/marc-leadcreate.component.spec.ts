import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcLeadcreateComponent } from './marc-leadcreate.component';

describe('MarcLeadcreateComponent', () => {
  let component: MarcLeadcreateComponent;
  let fixture: ComponentFixture<MarcLeadcreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcLeadcreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcLeadcreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
