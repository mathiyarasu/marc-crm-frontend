import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../services/notification.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Constants } from '../services/constants';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  public lovUrl = "product/getListOfNames/";
  public productSaveUrl = "product/save"
  productForm: FormGroup;
  submitted = false;
  public paraMeters = ["brand", "category", "color","discount"];
  public listOfValues = [];
  public brand = [];
  public category = [];
  public color = [];
  public discount = [];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService
  ) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      productName:["", Validators.required],
      productDesc:["", Validators.required],
      productCatName:["", Validators.required],
      brandName:["", Validators.required],
      productPrice:["", Validators.required],
      productSize:["", Validators.required],
      productWeight:["", Validators.required],
      productColor:["", Validators.required],
      productDicountAva:["", Validators.required],
      productDiscount:["", Validators.required],
      productInStock:["", Validators.required],
      unitsInStock:["", Validators.required],
      productImg:["", Validators.required]
    })
    const apiUrl = Constants.baseUrl + this.lovUrl + this.paraMeters;
    this.http.get(apiUrl).subscribe(
      (data) => {
        if (data['status'] === 'success') {
          this.listOfValues = data['payload'];
          this.brand = this.listOfValues.filter(list => list.lovCategory === 'brand');
          this.category = this.listOfValues.filter(list => list.lovCategory === 'category');
          this.color = this.listOfValues.filter(list => list.lovCategory === 'color');
          this.discount = this.listOfValues.filter(list => list.lovCategory === 'discount');

        }
      }
    )
  }

  get formControl(){
    return this.productForm.controls;
  }

  addProduct(){
    this.submitted = true;
    if(this.productForm.valid){
      const apiUrl = Constants.baseUrl + this.productSaveUrl;
      this.http.post(apiUrl,this.productForm.value).subscribe(
        (data)=>{
          if(data['status'] === 'success'){
            this.notifyService.showSuccess(data['message'],"");
            this.submitted = false;
            this.productForm.reset();
          }
        }
      )
    }

  }
}
