import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../services/notification.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Constants } from '../services/constants';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  public registerUrl = "user/register";
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private notifyService: NotificationService) { }
  ngOnInit() {
    this.registerForm = this.fb.group({
      userFirstName: ['', Validators.required],
      userLastName:  ['', Validators.required],
      userPassword: ['', [Validators.required,Validators.minLength(6)]],
      userEmail:  ['', [Validators.required,Validators.email]],
      userPhone:  ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]]
    })
  }

  get formControl(){
    return this.registerForm.controls;
  }
  register() {
    this.submitted = true;
    const apiUrl = Constants.baseUrl + this.registerUrl;
    this.http.post(apiUrl,this.registerForm.value).subscribe(
      (data)=>{
        if(data['status'] === 'success'){
          this.notifyService.showSuccess(data['message'],"");
          this.router.navigateByUrl("/login");
        }
      }
    )
  }
}
