import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcSidebarComponent } from './marc-sidebar.component';

describe('MarcSidebarComponent', () => {
  let component: MarcSidebarComponent;
  let fixture: ComponentFixture<MarcSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
